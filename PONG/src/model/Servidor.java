package model;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public abstract class Servidor {
	
	protected int numClientes;
	protected ServerSocket serverSocket;
	protected List<PrintWriter> listaClientesEscrita;
	
	Servidor(){
		
		listaClientesEscrita = new ArrayList<PrintWriter>();
		this.numClientes = 0;
	}
	
	public void addListaClientes(Socket socket) throws IOException{
		this.numClientes++;
		PrintWriter pw = new PrintWriter(socket.getOutputStream(),true);
		listaClientesEscrita.add(pw);
	}
	
	
	public void setServerSocket(ServerSocket serverSocket) {
		this.serverSocket = serverSocket;
	}

	public ServerSocket getServerSocket() {
		return serverSocket;
	}

	public void setNumClientes(int numClientes) {
		this.numClientes = numClientes;
	}

	public int getNumClientes() {
		return numClientes;
	}

	public void setListaClientes(List<PrintWriter> listaClientes) {
		this.listaClientesEscrita = listaClientes;
	}

	public List<PrintWriter> getListaClientes() {
		return listaClientesEscrita;
	}
	
}
