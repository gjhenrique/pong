package model;

import java.io.IOException;
import java.net.ServerSocket;

import javax.swing.JOptionPane;

public class ServidorChat extends Servidor{
	
	public ServidorChat() {
		
		super();
		
		try {
			this.setServerSocket(new ServerSocket(OpcoesAcesso.PORT_CHAT));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Ocorreu um erro ao criar o servidor",
					"ERRO",
				    JOptionPane.ERROR_MESSAGE);
		}
	}
		
}
