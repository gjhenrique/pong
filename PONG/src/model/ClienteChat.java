package model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import javax.swing.JOptionPane;
import javax.swing.JTextArea;

public class ClienteChat extends Cliente{
	
	private Socket socket;
	private PrintWriter printWriter;
	private BufferedReader bufferedReader;
	
	public ClienteChat(Socket socket){
		
		this.socket = socket;
		try {
			this.printWriter = new PrintWriter(this.socket.getOutputStream(), true);
		
			this.bufferedReader = new BufferedReader(new InputStreamReader(this.socket.getInputStream()));
		
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Ocorreu um erro em sua entrada",
					"ERRO",
				    JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void enviarNovaMensagem(String msg){
		this.printWriter.println(msg);
	}
	
	public void receberNovasMensagens(JTextArea areaMensagens){
		String linha = null;
		try {
		while((linha = this.bufferedReader.readLine()) != null){
				    areaMensagens.append("\n"+linha);
			}
		} catch (IOException e) {
				JOptionPane.showMessageDialog(null,"Ocorreu um erro ao ler novas mensagens",
						"ERRO",
					    JOptionPane.ERROR_MESSAGE);
			}
	}
	
}
