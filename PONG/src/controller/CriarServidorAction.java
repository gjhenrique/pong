package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import view.JanelaPrincipal;

public class CriarServidorAction implements ActionListener{

	private JanelaPrincipal framePrincipal;
	
	public CriarServidorAction(JanelaPrincipal framePrincipal){
		this.framePrincipal = framePrincipal;
	}
	
	public void actionPerformed(ActionEvent e) { 
		
		new ServidorChatHandler();
		new ClienteChatHandler(framePrincipal.getChatPanel(),"127.0.0.1");
		
		this.framePrincipal.iniciarJogo();
		
	}

}
