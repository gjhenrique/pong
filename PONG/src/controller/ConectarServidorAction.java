package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import view.JanelaPrincipal;

public class ConectarServidorAction implements ActionListener{

	private JanelaPrincipal janelaPrincipal;
	
	public ConectarServidorAction(JanelaPrincipal janelaPrincipal){
		this.janelaPrincipal = janelaPrincipal;
	}
	
	public void actionPerformed(ActionEvent e) {
		
		String numIP;
		numIP = JOptionPane.showInputDialog("IP do usuario");
		new ClienteChatHandler(janelaPrincipal.getChatPanel(),numIP);
		
		this.janelaPrincipal.iniciarJogo();
	}
}