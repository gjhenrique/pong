package controller;

import java.io.IOException;
import java.net.Socket;

import javax.swing.JOptionPane;

import model.ServidorChat;

public class ServidorChatHandler implements Runnable{
	
	private ServidorChat servidorChat;
	
	public ServidorChatHandler(){
		this.servidorChat = new ServidorChat();

		new Thread(this).start();
		
	}
	
	public void run() {
			try {
				
				Socket socketCliente = null;
				
				while(this.servidorChat.getNumClientes() < 2){
				socketCliente = this.servidorChat.getServerSocket().accept();
				
				this.servidorChat.addListaClientes(socketCliente);
				
				new Thread(new EnvioMensagensChat(servidorChat, socketCliente,this.servidorChat.getNumClientes())).start();
				}
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null,"Ocorreu um erro ao aceitar novos usuários",
						"ERRO",
					    JOptionPane.ERROR_MESSAGE);
			}
		}
		
	
	
}
