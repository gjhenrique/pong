package controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

import javax.swing.JOptionPane;

import model.ServidorChat;

public class EnvioMensagensChat implements Runnable{
	
	private ServidorChat servidor;
	
	private BufferedReader bufferedReader;
	
	int numCliente;
	
	public EnvioMensagensChat(ServidorChat servidor, Socket socket,int numCliente) throws IOException{
		this.servidor = servidor;
		
		this.numCliente = numCliente;
		
		try {
			this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Ocorreu um erro no servidor ao criar o fluxo do novo cliente",
					"ERRO",
				    JOptionPane.ERROR_MESSAGE);
		}
	}
	
	public void run() {
		
		this.broadcast("Conectado no Servidor");
	
		String linha = null;
		try {
		while((linha = bufferedReader.readLine()) != null){
					
				    this.broadcast(linha);
			}
		} catch (IOException e) {
				JOptionPane.showMessageDialog(null,"Ocorreu um erro ao ler novas mensagens",
						"ERRO",
					    JOptionPane.ERROR_MESSAGE);
			}
	}
		

	private void broadcast(String msg){
		String mensagem = "Player"+this.numCliente + ":  "+msg;
		
		for(int i = 0; i < this.servidor.getNumClientes(); i++){
			this.servidor.getListaClientes().get(i).println(mensagem);
		}
	}
	
}
