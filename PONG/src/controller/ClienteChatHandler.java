package controller;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JOptionPane;

import model.ClienteChat;
import model.OpcoesAcesso;
import view.ChatPanel;

public class ClienteChatHandler implements Runnable,KeyListener{
	
	private Socket socket;
	
	private ClienteChat cliente;
	
	private ChatPanel chatPanel;
	
	public ClienteChatHandler(ChatPanel chatPanel, String ip){
		this.chatPanel = chatPanel;
		this.chatPanel.getCampoMensagem().addKeyListener(this);
		
		try {
			this.socket = new Socket(ip,OpcoesAcesso.PORT_CHAT);
			
			this.cliente = new ClienteChat(socket);
			
			new Thread(this).start();
			
		} catch (UnknownHostException e) {
			
			JOptionPane.showMessageDialog(null,"Não há servidores com esse numero de IP",
					"ERRO",
				    JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null,"Ocorreu um erro ao manipular mensagens do cliente",
					"ERRO",
				    JOptionPane.ERROR_MESSAGE);
		}
	}

	public void run() {
		cliente.receberNovasMensagens(this.chatPanel.getAreaMensagens());
	}

	
	public void keyPressed(KeyEvent event) {
		String msg = null;
		
		if(event.getKeyCode() == KeyEvent.VK_ENTER)  
	      {  
	         msg = this.chatPanel.getCampoMensagem().getText();
	         cliente.enviarNovaMensagem(msg);
	         this.chatPanel.getCampoMensagem().setText("");
	      }  
		
	}

	@Override
	public void keyReleased(KeyEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent event) {
		/*String msg = null;
		
		if(event.getKeyCode() == KeyEvent.VK_ENTER)  
	      {  
	         msg = this.chatPanel.getCampoMensagem().getText();
	         cliente.enviarNovaMensagem(msg);
	         this.chatPanel.getCampoMensagem().setText("");
	      } */ 
	}
	
}
