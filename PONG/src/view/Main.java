package view;

import java.awt.EventQueue;

import javax.swing.UIManager;

public class Main {
public static void main(String[] args) {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable(){
			public void run(){
				new JanelaPrincipal();
			}
		});
	}
}
