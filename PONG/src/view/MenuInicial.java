package view;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MenuInicial extends JMenuBar{
	
	
	private static final long serialVersionUID = 1L;
	private JMenuItem itemCriarServidor;
	private JMenuItem itemConectarServidor;
	
	public MenuInicial(){
		this.iniciarMenu();
	}
	
	private void iniciarMenu(){
		JMenu menuJogar = new JMenu("Jogar");
		this.add(menuJogar); 
		
		this.itemCriarServidor = new JMenuItem("Criar Servidor");
		menuJogar.add(itemCriarServidor);
		
		menuJogar.addSeparator();
		
		this.itemConectarServidor = new JMenuItem("Conectar-se a um Servidor");
		menuJogar.add(itemConectarServidor);
		
	}

	public JMenuItem getItemConectarServidor() {
		return itemConectarServidor;
	}

	public void setItemConectarServidor(JMenuItem itemConectarServidor) {
		this.itemConectarServidor = itemConectarServidor;
	}

	public JMenuItem getItemCriarServidor() {
		return itemCriarServidor;
	}

	public void setItemCriarServidor(JMenuItem itemCriarServidor) {
		this.itemCriarServidor = itemCriarServidor;
	}
}
