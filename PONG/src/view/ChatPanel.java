package view;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.LineBorder;

public class ChatPanel extends JPanel{

	private static final long serialVersionUID = 1L;
	
	private JTextField campoMensagem; //Area de escrita do usuario
	private JTextArea areaMensagens; //Area de exibição das mensagens
	private JLabel labelChat;
	
	public ChatPanel(){
		
		this.campoMensagem = new JTextField(20);

		this.labelChat = new JLabel("CHAT",JLabel.CENTER);
		this.labelChat.setFont(new Font("Arial", Font.BOLD, 20));  
		
		this.areaMensagens = new JTextArea();
		this.areaMensagens.setEditable(false);
		this.areaMensagens.setBorder(new LineBorder(Color.gray));
		
		this.iniciarPainel();
	}
	
	private void iniciarPainel(){
		setSize(200, 600);
		this.setLayout(new BorderLayout(5,20));
		this.add(labelChat,BorderLayout.NORTH);
		
		this.add(areaMensagens,BorderLayout.CENTER);
		
		this.add(campoMensagem,BorderLayout.SOUTH);
	}
	
	public JTextField getCampoMensagem() {
		return campoMensagem;
	}

	public void setCampoMensagem(JTextField campoMensagem) {
		this.campoMensagem = campoMensagem;
	}

	public JTextArea getAreaMensagens() {
		return areaMensagens;
	}

	public void setAreaMensagens(JTextArea areaMensagens) {
		this.areaMensagens = areaMensagens;
	}
}
