package view;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

 public class ImagePanel extends JPanel {  
        /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
		private final Image image;  
          
        public ImagePanel(Image _image) {  
            image = _image;  
        }  
          
        @Override  
        protected void paintComponent(Graphics g) {  
            super.paintComponent(g);  
            if (image != null)   
                g.drawImage(image, 0, 0, getWidth(), getHeight(), this);  
        }  
    }  