package view;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

public class JogoPanel extends JPanel{
	
	private ImageIcon imgPong;
	
	JogoPanel(){
		initPainel();
	}
	
	private void initPainel(){
		
		//this.setBackground(Color.black);
		imgPong = new ImageIcon("img/pong.jpg");
	}
	
	public void paintComponent(Graphics g) {
			Image img = imgPong.getImage();
	        g.drawImage(img, 20, 10, null);
	        
	    }
	private static final long serialVersionUID = 1L;
	
}
