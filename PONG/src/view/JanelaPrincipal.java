package view;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

import controller.ConectarServidorAction;
import controller.CriarServidorAction;

public class JanelaPrincipal extends JFrame{

	
	private static final long serialVersionUID = 1L;

	private JogoPanel jogoPanel;	
	private ChatPanel chatPanel;
	private MenuInicial menuInicio;
	private MenuJogo menuJogo;
	
	public JanelaPrincipal(){
		this.iniciarJanela();
	}
	
	public void iniciarJanela(){
		
		this.jogoPanel = new JogoPanel();
		this.jogoPanel.setPreferredSize(new Dimension(800,540));
		this.add(jogoPanel);
		this.jogoPanel.setVisible(false);
		
		this.chatPanel = new ChatPanel();
		this.chatPanel.setPreferredSize(new Dimension(250, 540));
		this.add(chatPanel);
		this.chatPanel.setVisible(false);
		
		this.menuInicio = new MenuInicial();
		this.menuInicio.getItemCriarServidor().addActionListener(new CriarServidorAction(this));
		this.menuInicio.getItemConectarServidor().addActionListener(new ConectarServidorAction(this));
		this.setJMenuBar(this.menuInicio);
		
		this.menuJogo = new MenuJogo();
		
		ImageIcon imgPong = new ImageIcon("img/icone_pong.jpg");
		this.setIconImage(imgPong.getImage());
		
		this.setTitle("PONG");
		
		//this.fecharAplicacao();
		
		this.setVisible(true);
		
		//this.setBounds(300, 100, 0, 0);
		this.setSize(1100,600);
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(new FlowLayout());
		
	}
	/*private void fecharAplicacao(){
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent arg0) {
				int resposta = JOptionPane.showConfirmDialog(null, "Deseja encerrar o Jogo?",
				"Confirmação", JOptionPane.OK_CANCEL_OPTION);
//				se a resposta for sim, encerra a aplicação
				if (resposta == JOptionPane.OK_OPTION){
					setDefaultCloseOperation(EXIT_ON_CLOSE);
				}
			}
		});
	}*/
	
	public void iniciarJogo(){
		this.chatPanel.setVisible(true);
		this.jogoPanel.setVisible(true);
		this.setJMenuBar(this.menuJogo);
		
	}
	
	public JogoPanel getJogoPanel() {
		return jogoPanel;
	}

	public void setJogoPanel(JogoPanel jogoPanel) {
		this.jogoPanel = jogoPanel;
	}

	public ChatPanel getChatPanel() {
		return chatPanel;
	}

	public void setChatPanel(ChatPanel chatPanel) {
		this.chatPanel = chatPanel;
	}
	
	
}

	
