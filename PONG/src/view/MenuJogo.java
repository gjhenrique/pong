package view;

import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class MenuJogo extends JMenuBar{

	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JMenuItem itemDesconectar;

	public MenuJogo(){
		iniciarMenu();
	}
	
	private void iniciarMenu(){
		
		JMenu menuDesconectar = new JMenu("Desconectar");
		this.add(menuDesconectar);
		
		this.itemDesconectar = new JMenuItem("Sair do Jogo");
		menuDesconectar.add(itemDesconectar);
	}

	public JMenuItem getItemDesconectar() {
		return itemDesconectar;
	}

	public void setItemDesconectar(JMenuItem itemDesconectar) {
		this.itemDesconectar = itemDesconectar;
	}
}
